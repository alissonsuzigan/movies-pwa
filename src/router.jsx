import React from 'react';
import { /* Switch, */ HashRouter, Route } from 'react-router-dom';
// Components
import Header from 'components/header';
// Views
import Home from 'routes/home';
import About from 'routes/about';
import 'assets/styles/global.scss';

// Router :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
export default (
  <HashRouter>
    <div id="page-content">
      <Header />
      <main id="main" className="main">
        <Route exact path="/" component={Home} />
        <Route path="/home" component={Home} />
        <Route path="/about" component={About} />
      </main>
    </div>
  </HashRouter>
);
