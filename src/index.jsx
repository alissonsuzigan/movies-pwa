import React from 'react';
import { render } from 'react-dom';
// Redux
import { Provider } from 'react-redux';
import { createStore, applyMiddleware } from 'redux';
import thunk from 'redux-thunk';
import reducers from 'stores';
// Router
import router from './router';


const store = createStore(reducers, applyMiddleware(thunk));
const app = (
  <Provider store={store}>
    {router}
  </Provider>
);

render(app, document.querySelector('#application'));
