const path = require('path');
// Global paths
const buildPath = path.resolve(process.cwd(), 'build');
const configPath = path.resolve(process.cwd(), 'config');
const sourcePath = path.resolve(process.cwd(), 'src');
const publicPath = 'http://localhost:8888/';

// Loaders and plugins
const autoprefixer = require('autoprefixer');
const ExtractCSS = require('extract-text-webpack-plugin');
const HtmlPlugin = require('html-webpack-plugin');
const StyleLint = require('stylelint-webpack-plugin');
const webpack = require('webpack');

// Local configs
// const appVersion = require('../../package.json').version;
const pkg = require('../../package.json');
const appConfig = require('../env/dev.json');


// Webpack configs ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
module.exports = {
  context: sourcePath,
  devtool: 'eval',

  entry: {
    app: ['index.jsx'],
    vendor: [
      'prop-types',
      'react',
      'react-dom',
      'react-redux',
      'react-router-dom',
      'redux',
      'redux-thunk'
    ]
  },

  output: {
    filename: 'static/js/[chunkhash:8].[name].js',
    path: buildPath,
    publicPath
  },

  resolve: {
    extensions: ['.js', '.jsx', '.scss'],
    alias: {
      assets: `${sourcePath}/assets`,
      components: `${sourcePath}/components`,
      routes: `${sourcePath}/routes`,
      stores: `${sourcePath}/stores`
    },
    modules: [
      'node_modules',
      sourcePath
    ]
  },


  // dev-server configs :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
  devServer: {
    contentBase: sourcePath,
    compress: true,
    port: 8888,
    // open: true,
    stats: {
      assets: true,
      children: false,
      modules: false
    }
  },


  // Modules ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
  module: {
    rules: [
      { // eslint config
        test: /\.jsx?$/,
        enforce: 'pre',
        include: sourcePath,
        loader: 'eslint-loader',
        options: {
          configFile: `${configPath}/.eslintrc`
          // fix: true // autofix eslint errors
        }
      },

      { // babel config
        test: /\.jsx?$/,
        include: sourcePath,
        use: [
          'babel-loader'
        ]
      },

      { // scss config
        test: /\.scss$/,
        include: sourcePath,
        use: ExtractCSS.extract({
          fallback: 'style-loader',
          use: [
            'css-loader',
            {
              loader: 'postcss-loader',
              options: {
                plugins: [autoprefixer]
              }
            },
            'sass-loader'
          ]
        })
      }

    ]
  },


  // Plugins ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
  plugins: [
    // Extract CSS
    new ExtractCSS('static/css/[contenthash:8].[name].css'),

    // Generate HTML file
    new HtmlPlugin({
      template: 'index.html'
      // minify: {
      //   collapseWhitespace: true,
      //   removeComments: true,
      //   removeRedundantAttributes: true,
      //   removeScriptTypeAttributes: true,
      //   removeStyleLinkTypeAttributes: true
      // }
    }),

    // Lint scss files
    new StyleLint({
      configFile: `${configPath}/.stylelintrc/`,
      context: sourcePath,
      syntax: 'scss'
    }),

    // Add global object config
    new webpack.DefinePlugin({
      APP: {
        CONFIG: JSON.stringify(appConfig),
        ENV: JSON.stringify('dev'),
        VERSION: JSON.stringify(pkg.version)
      }
    }),

    // Build separeted vendor file
    new webpack.optimize.CommonsChunkPlugin({
      name: 'vendor',
      minChunks: Infinity
    }),

    // Minify CSS
    new webpack.LoaderOptionsPlugin({
      minimize: true
    })

    // Minify JS
    // new webpack.optimize.UglifyJsPlugin({
    //   sourceMap: false,
    //   compress: true
    // }),
    // Minify scripts
    // new webpack.optimize.UglifyJsPlugin()

  ]
};
